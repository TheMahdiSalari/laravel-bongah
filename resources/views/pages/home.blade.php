@extends('layout')

@section('content')

    <main class="container">
        <div class="bg-light p-5 rounded">
            <h1>Bongah</h1>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A deserunt dolor, doloribus eius exercitationem
                facere, fugiat fugit in ipsam laboriosam magni minima provident quae reprehenderit, similique totam vel
                voluptate voluptatibus?</p>
            <a class="btn btn-lg btn-primary" href="{{ route('banners.create') }}" role="button">Create Banner
                &raquo;</a>
        </div>
    </main>

@stop
