@inject('countries','\App\Http\Utilities\Country')

@csrf

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label for="street">Street</label>
            <input type="text" class="form-control" name="street" id="street" value="{{ old("street") }}" required>
        </div>

        <div class="form-group">
            <label for="city">City</label>
            <input type="text" class="form-control" name="city" id="city" value="{{ old("city") }}" required>
        </div>

        <div class="form-group">
            <label for="zip">Zip / Post Code</label>
            <input type="text" class="form-control" name="zip" id="zip" value="{{ old("zip") }}" required>
        </div>

        <div class="form-group">
            <label for="country">Country</label>
            <select name="country" id="country" class="form-control">
                @foreach($countries::all() as $country)
                    <option value="{{ $country }}">{{ $country }}</option>
                @endforeach
            </select>
        </div>

        <div class="form-group">
            <label for="state">State</label>
            <input type="text" class="form-control" name="state" id="state" value="{{ old("state") }}" required>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="price">Selling Price</label>
            <input type="text" class="form-control" name="price" id="price" value="{{ old("price") }}" required>
        </div>

        <div class="form-group">
            <label for="description">Description Home</label>
            <textarea name="description" id="description" rows="9" class="form-control"
                      required>{{ old('description') }}</textarea>
        </div>
    </div>
</div>
<br>
<div>
    <button type="submit" class="btn btn-primary">Create Banner</button>
</div>
